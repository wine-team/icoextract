#!/bin/bash

HELP2MAN_ARGS=(--no-info --include ./help2man.include)

help2man icoextract      --name 'extract icons from Windows PE files'    "${HELP2MAN_ARGS[@]}" -o icoextract.1
help2man icolist         --name 'list icons present in Windows PE files' "${HELP2MAN_ARGS[@]}" -o icolist.1
help2man exe-thumbnailer --name 'thumbnailer for Windows PE files'       "${HELP2MAN_ARGS[@]}" -o exe-thumbnailer.1
sed -i 's/EXE-THUMBNAILER,/EXE-THUMBNAILER/g' exe-thumbnailer.1
sed -i 's/-thumbnailer,/-thumbnailer/' exe-thumbnailer.1

